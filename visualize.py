#!/usr/bin/python3

from datetime import datetime
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

# Data for plotting

times = []
votes = []

def parse_line(f):
    s = f.readline().rstrip('\n')
    tokens = s.split(' ')
    if len(tokens) != 3:
        return False
    print (tokens)
    datestring = ' '.join([tokens[0],tokens[1]])
    vote_number = int(tokens[2]) 
    votes.append(vote_number)
    # 2020-05-14 12:30:56.382991
    t = datetime.fromisoformat(datestring)
    times.append(t)
    return True



with open("./vote-log","r") as f:
    while parse_line(f):
        pass

fig, ax = plt.subplots()
ax.plot(times, votes)

ax.set(xlabel='time (UTC)', ylabel='votes',
       title='5 steps vote curve')
ax.grid()

fig.savefig("5s_graph.png")
#plt.show()
