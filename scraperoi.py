#!/bin/python3

from bs4 import BeautifulSoup
import subprocess
import os
import datetime

def _removeNonAscii(s): return "".join(chr(i) for i in s if i<128)

def get_text():
    p = subprocess.Popen('torsocks -i curl https://www.roi.ru/66369/', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    res = ''
    for line in p.stdout.readlines():
        res+=_removeNonAscii(line).strip('\n')
    
    retval = p.wait()
    return res

text = get_text()
soup = BeautifulSoup(text,'html.parser')
votes = soup.find(attrs={"class":"status js-b-voting-result"})['data-vote-affirmative']
print (datetime.datetime.now(), votes)
