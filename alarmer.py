#!/usr/bin/python

from datetime import datetime

prev_value = 0
prev_tokens = []

def parse_line(f):
    global prev_value
    global prev_tokens

    s = f.readline().rstrip('\n')
    tokens = s.split(' ')
    if len(tokens) != 3:
        return False
    datestring = ' '.join([tokens[0],tokens[1]])
    vote_number = int(tokens[2]) 
    # 2020-05-14 12:30:56.382991
    t = datetime.fromisoformat(datestring)

    if vote_number < prev_value:
        print ('--------------------------')
        print (prev_tokens)
        print (tokens)
        print ('ALARM!!!')
    prev_value = vote_number
    prev_tokens = tokens 
    return True



with open("./vote-log","r") as f:
    while parse_line(f):
        pass
